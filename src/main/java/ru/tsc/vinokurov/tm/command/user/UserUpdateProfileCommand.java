package ru.tsc.vinokurov.tm.command.user;

import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-update-profile";

    public static final String DESCRIPTION = "Update user profile.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        final String userId = getAuthService().getUserId();
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
