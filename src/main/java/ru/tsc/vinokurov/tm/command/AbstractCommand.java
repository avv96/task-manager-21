package ru.tsc.vinokurov.tm.command;

import ru.tsc.vinokurov.tm.api.model.ICommand;
import ru.tsc.vinokurov.tm.api.service.IAuthService;
import ru.tsc.vinokurov.tm.api.service.IServiceLocator;
import ru.tsc.vinokurov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public abstract void execute();

    public abstract Role[] getRoles();

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String description = getDescription();
        final String argument = getArgument();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
