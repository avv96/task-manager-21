package ru.tsc.vinokurov.tm.command.system;

import ru.tsc.vinokurov.tm.enumerated.Role;

public final class AboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String DESCRIPTION = "Show developer info.";

    public static final String ARGUMENT = "-a";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Aleksey Vinokurov");
        System.out.println("E-mail: avvinokurov@t1-consulting.ru");
    }

}
