package ru.tsc.vinokurov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.vinokurov.tm.api.service.IUserOwnedService;
import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.exception.entity.ItemNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.IdEmptyException;
import ru.tsc.vinokurov.tm.exception.field.IndexIncorrectException;
import ru.tsc.vinokurov.tm.exception.field.UserIdEmptyException;
import ru.tsc.vinokurov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (comparator == null) findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }

    @Override
    public M add(final String userId, final M item) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (item == null) throw new ItemNotFoundException();
        return repository.add(userId, item);
    }

    @Override
    public M remove(final String userId, final M item) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        return repository.remove(userId, item);
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final M item = Optional.ofNullable(findOneById(id)).orElseThrow(ItemNotFoundException::new);
        return repository.remove(userId, item);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final M item = Optional.ofNullable(findOneByIndex(index)).orElseThrow(ItemNotFoundException::new);
        return repository.remove(userId, item);
    }

    @Override
    public void clear(final String userId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.size()) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(id)) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public int size(final String userId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        return repository.size(userId);
    }

}
