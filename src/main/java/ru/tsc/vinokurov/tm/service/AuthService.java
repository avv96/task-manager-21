package ru.tsc.vinokurov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.service.IAuthService;
import ru.tsc.vinokurov.tm.api.service.IUserService;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.exception.field.LoginEmptyException;
import ru.tsc.vinokurov.tm.exception.field.PasswordEmptyException;
import ru.tsc.vinokurov.tm.exception.system.AccessDeniedException;
import ru.tsc.vinokurov.tm.exception.system.PermissionException;
import ru.tsc.vinokurov.tm.model.User;
import ru.tsc.vinokurov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        final User user = Optional.ofNullable(userService.findOneByLogin(login)).orElseThrow(AccessDeniedException::new);
        if (!user.getPasswordHash().equals(HashUtil.salt(password)))
            throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User register(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        return userService.findOneById(getUserId());
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new PermissionException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role userRole = Optional.ofNullable(user.getRole()).orElseThrow(PermissionException::new);
        final boolean matchUserRole = Arrays.asList(roles).contains(userRole);
        if (!matchUserRole) throw new PermissionException();
    }

}
