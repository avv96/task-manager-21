package ru.tsc.vinokurov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.repository.IProjectRepository;
import ru.tsc.vinokurov.tm.api.service.IProjectService;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.*;
import ru.tsc.vinokurov.tm.model.Project;

import java.util.Date;
import java.util.Optional;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) {
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        if (StringUtils.isEmpty(description)) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Project create(final String userId, final String name, final String description, final Date dateBegin, final Date dateEnd) {
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        final Project project = Optional.ofNullable(create(userId, name, description))
                .orElseThrow(ProjectNotFoundException::new);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(id)) throw new ProjectIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(id)) throw new ProjectIdEmptyException();
        final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        return project;
    }

}
