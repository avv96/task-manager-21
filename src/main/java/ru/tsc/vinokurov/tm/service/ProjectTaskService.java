package ru.tsc.vinokurov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.repository.IProjectRepository;
import ru.tsc.vinokurov.tm.api.repository.ITaskRepository;
import ru.tsc.vinokurov.tm.api.service.IProjectTaskService;
import ru.tsc.vinokurov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.vinokurov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.vinokurov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.vinokurov.tm.exception.field.UserIdEmptyException;
import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    private IProjectRepository projectRepository;
    private ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isEmpty(taskId)) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId)).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isEmpty(taskId)) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId)).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
    }

    @Override
    public void removeProject(final String userId, final String projectId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(projectId)) throw new ProjectIdEmptyException();
        final Project project = Optional.ofNullable(projectRepository.findOneById(userId, projectId)).orElseThrow(ProjectNotFoundException::new);
        projectRepository.remove(userId, project);
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.forEach(task -> taskRepository.remove(userId, task));
    }

    @Override
    public void removeProject(final String userId, final Project project) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(userId, project);
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, project.getId());
        tasks.forEach(task -> taskRepository.remove(userId, task));
    }

}
