package ru.tsc.vinokurov.tm.component;

import ru.tsc.vinokurov.tm.api.repository.ICommandRepository;
import ru.tsc.vinokurov.tm.api.repository.IProjectRepository;
import ru.tsc.vinokurov.tm.api.repository.ITaskRepository;
import ru.tsc.vinokurov.tm.api.repository.IUserRepository;
import ru.tsc.vinokurov.tm.api.service.*;
import ru.tsc.vinokurov.tm.command.AbstractCommand;
import ru.tsc.vinokurov.tm.command.binding.BindTaskToProject;
import ru.tsc.vinokurov.tm.command.binding.UnbindTaskFromProject;
import ru.tsc.vinokurov.tm.command.project.*;
import ru.tsc.vinokurov.tm.command.system.*;
import ru.tsc.vinokurov.tm.command.task.*;
import ru.tsc.vinokurov.tm.command.user.*;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.vinokurov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.model.Task;
import ru.tsc.vinokurov.tm.repository.CommandRepository;
import ru.tsc.vinokurov.tm.repository.ProjectRepository;
import ru.tsc.vinokurov.tm.repository.TaskRepository;
import ru.tsc.vinokurov.tm.repository.UserRepository;
import ru.tsc.vinokurov.tm.service.*;
import ru.tsc.vinokurov.tm.util.DateUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registerCommand(new InfoCommand());
        registerCommand(new VersionCommand());
        registerCommand(new HelpCommand());
        registerCommand(new AboutCommand());
        registerCommand(new ArgumentsCommand());
        registerCommand(new CommandsCommand());
        registerCommand(new TaskListCommand());
        registerCommand(new TaskChangeStatusByIdCommand());
        registerCommand(new TaskChangeStatusByIndexCommand());
        registerCommand(new TaskClearCommand());
        registerCommand(new TaskCompleteByIdCommand());
        registerCommand(new TaskCompleteByIndexCommand());
        registerCommand(new TaskCreateCommand());
        registerCommand(new TaskRemoveByIdCommand());
        registerCommand(new TaskRemoveByIndexCommand());
        registerCommand(new TaskShowByIdCommand());
        registerCommand(new TaskShowByIndexCommand());
        registerCommand(new TaskStartByIdCommand());
        registerCommand(new TaskStartByIndexCommand());
        registerCommand(new TaskUpdateByIdCommand());
        registerCommand(new TaskUpdateByIndexCommand());
        registerCommand(new ProjectListCommand());
        registerCommand(new ProjectChangeStatusByIdCommand());
        registerCommand(new ProjectChangeStatusByIndexCommand());
        registerCommand(new ProjectClearCommand());
        registerCommand(new ProjectCompleteByIdCommand());
        registerCommand(new ProjectCompleteByIndexCommand());
        registerCommand(new ProjectCreateCommand());
        registerCommand(new ProjectRemoveByIdCommand());
        registerCommand(new ProjectRemoveByIndexCommand());
        registerCommand(new ProjectShowByIdCommand());
        registerCommand(new ProjectShowByIndexCommand());
        registerCommand(new ProjectStartByIdCommand());
        registerCommand(new ProjectStartByIndexCommand());
        registerCommand(new ProjectUpdateByIdCommand());
        registerCommand(new ProjectUpdateByIndexCommand());
        registerCommand(new BindTaskToProject());
        registerCommand(new UnbindTaskFromProject());
        registerCommand(new UserChangePasswordCommand());
        registerCommand(new UserLoginCommand());
        registerCommand(new UserLogoutCommand());
        registerCommand(new UserRegisterCommand());
        registerCommand(new UserUpdateProfileCommand());
        registerCommand(new UserViewProfileCommand());
        registerCommand(new ExitCommand());
    }

    public void close() {
        System.exit(0);
    }

    public void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    private void initUsers() {
        userService.create("user", "user");
        userService.create("test", "test", "test@example.com");
        userService.create("admin", "admin", "admin@example.com", Role.ADMIN);
    }

    public void processCommand() {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String command;
            System.out.print("Enter command: ");
            while ((command = reader.readLine()) != null) {
                try {
                    loggerService.command(command);
                    processCommand(command);
                    System.out.println("[OK]");
                } catch (final Exception e) {
                    loggerService.error(e);
                    System.err.println("[FAIL]");
                }
                System.out.print("Enter command: ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void processArgument(final String arg) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length < 1) return false;
        for (String arg : args) {
            try {
                processArgument(arg);
                System.out.println("[OK]");
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
        return true;
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **")));
    }

    public void run(final String[] args) {
        if (processArgument(args)) close();
        initDemoData();
        initLogger();
        initUsers();
        processCommand();
    }

    private void registerCommand(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() {
        taskService.create("DEMO TASK 0", "DESC #0");
        taskService.create("DEMO TASK 1", "DESC #2");
        taskService.create("DEMO TASK 2", "DESC #3");
        projectService.create("DEMO Project 0", "DESC #0");
        projectService.create("DEMO Project 1", "DESC #1");
        projectService.create("DEMO Project 2", "DESC #2");
    }

    private void initDemoData() {
        projectService.add(new Project("DEMO PROJECT", Status.IN_PROGRESS, DateUtil.toDate("2020.10.27")));
        projectService.add(new Project("TEST PROJECT", Status.COMPLETED, DateUtil.toDate("2018.10.27")));
        projectService.add(new Project("BEST PROJECT", Status.NOT_STARTED, DateUtil.toDate("2019.10.27")));
        taskService.add(new Task("GAMMA TASK 0", Status.COMPLETED, DateUtil.toDate("2029.10.27")));
        taskService.add(new Task("ALPHA TASK 1", Status.IN_PROGRESS, DateUtil.toDate("2018.10.27")));
        taskService.add(new Task("BETTA TASK 2", Status.IN_PROGRESS, DateUtil.toDate("2021.10.27")));
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
