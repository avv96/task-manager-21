package ru.tsc.vinokurov.tm.api.service;

import ru.tsc.vinokurov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId);

    void unbindTaskFromProject(String userId, String projectId, String taskId);

    void removeProject(String userId, String projectId);

    void removeProject(String userId, Project project);
}
