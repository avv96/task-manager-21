package ru.tsc.vinokurov.tm.api.model;

import ru.tsc.vinokurov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
