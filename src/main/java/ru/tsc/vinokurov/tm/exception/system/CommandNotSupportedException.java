package ru.tsc.vinokurov.tm.exception.system;

import ru.tsc.vinokurov.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command is not supported...");
    }

    public CommandNotSupportedException(String command) {
        super("Error! Command ``" + command + "`` not supported...");
    }
}
